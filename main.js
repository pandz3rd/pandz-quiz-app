// List quiz
const quizData = [
  {
    question: 'Buwong apa tu man?',
    a: 'Ayam',
    b: 'Bebek',
    c: 'Itik',
    d: 'Puyoh',
    correct: 'd'
  },
  {
    question: 'Siapa yang menulis Gymnopedies?',
    a: 'Debussy',
    b: 'Erik Satie',
    c: 'Chopin',
    d: 'Gorgeous Ludovico',
    correct: 'b'
  },
  {
    question: 'Tanaman air asli Indonesia',
    a: 'Buchepalandra',
    b: 'Anubias',
    c: 'Rotala',
    d: 'Duckweed',
    correct: 'a'
  },
  {
    question: 'Kura-kura asli Indonesia',
    a: 'Kura Brazil',
    b: 'Kura Ambon',
    c: 'Kura Dada Merah',
    d: 'Kura Siebenrocki',
    correct: 'b'
  },
  {
    question: 'Aquarium dan lanscape adalah kepanjangan dari',
    a: 'Aquascape',
    b: 'Aqualand',
    c: 'Aquajaya',
    d: 'Aquair',
    correct: 'a'
  },
]

// Logic
let currentQuestion = 0
let score = 0

const questionText = document.getElementById('question')

let quiz = document.getElementById('quiz')
let answerEls = document.querySelectorAll('.answers')

const aText = document.getElementById('aText')
const bText = document.getElementById('bText')
const cText = document.getElementById('cText')
const dText = document.getElementById('dText')

const submitBtn = document.getElementById('submit')

const answer = undefined

const loadQuiz = () => {
  refreshAnswer()
  const currentQuiz = quizData[currentQuestion]
  questionText.innerText = currentQuiz.question
  aText.innerText = currentQuiz.a
  bText.innerText = currentQuiz.b
  cText.innerText = currentQuiz.c
  dText.innerText = currentQuiz.d
}

const getSelectedAnswer = () => {
  let answerValue = undefined

  answerEls.forEach(answerEl => {
    if (answerEl.checked) answerValue = answerEl.id
  })
  return answerValue
}

const refreshAnswer = () => {
  answerEls.forEach(answerEl => {
    answerEl.checked = false
  })
}

submitBtn.addEventListener('click', ()=> {
  const selectedAnswer = getSelectedAnswer()
  
  if (selectedAnswer) {
    if (selectedAnswer === quizData[currentQuestion].correct) {
      console.log(selectedAnswer, currentQuestion)
      score++
    }

    currentQuestion++
    if (currentQuestion < quizData.length) {
      loadQuiz()
    } else {
      quiz.innerHTML = `<h2>You answered correctly ${score} of ${quizData.length} questions</h2>
        <button onClick="location.reload()">Try Again</button>`
    }
  } else {
    alert('Pilih jawabanmu')
  }
})

loadQuiz()
